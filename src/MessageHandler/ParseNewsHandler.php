<?php

namespace App\MessageHandler;

use App\Entity\News;
use App\Message\ParseNews;
use App\Repository\NewsRepository;
use App\Service\NewsParsingService;
use App\Service\TruncateSentencesService;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class ParseNewsHandler implements MessageHandlerInterface
{

    private $logger;
    private $newsParser;

    public function __construct(NewsParsingService $newsParser, LoggerInterface $logger)
    {
        $this->newsParser = $newsParser;
        $this->logger = $logger;
    }

    public function __invoke(ParseNews $parseNewsCommand)
    {
        $this->newsParser->parseNews();
        $this->logger->info('Handler parsing news task complete.');

    }
}