<?php

namespace App\Controller;

use App\Entity\News;
use App\Repository\NewsRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

class NewsController extends AbstractController
{

    private $twig;
    private UrlGeneratorInterface $urlGenerator;

    public function __construct(Environment $twig, UrlGeneratorInterface $urlGenerator)
    {
        $this->twig = $twig;
        $this->urlGenerator = $urlGenerator;
    }

    /**
     * @Route("/", name="homepage")
     */
    public function index(): ?Response
    {

        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');

        $user = $this->getUser();
        $roles = $user->getRoles();
        if(in_array('ROLE_ADMIN', $roles)) {
            return $this->redirect($this->urlGenerator->generate('admin'));
        } elseif (in_array('ROLE_MODERATOR', $roles)) {
            return $this->redirect($this->urlGenerator->generate('news'));
        }
        return null;
    }

    /**
     * @Route("/news", name="news")
     */
    public function news(Request $request, NewsRepository $newsRepository): Response
    {
        $offset = max(0, $request->query->getInt('offset', 0));
        $paginator = $newsRepository->getNewsPaginator($offset);

        $user = $this->getUser();
        $roles = $user->getRoles();
        $is_admin = in_array('ROLE_ADMIN', $roles);
        return new Response($this->twig->render('news/index.html.twig', [
            'logged_in' => true,
            'is_admin' => $is_admin,
            'news' => $paginator,
            'previous' => $offset - NewsRepository::PAGINATOR_PER_PAGE,
            'current_page' => $offset / 10 + 1,
            'next' => min(count($paginator), $offset + NewsRepository::PAGINATOR_PER_PAGE),
        ]));
    }

    /**
     * @Route("/news/{id}", name="article")
     */
    public function show(News $article): Response
    {
        $user = $this->getUser();
        $roles = $user->getRoles();
        $is_admin = in_array('ROLE_ADMIN', $roles);
        return new Response($this->twig->render('news/show.html.twig', [
            'logged_in' => true,
            'is_admin' => $is_admin,
            'article' => $article,
        ]));
    }
}
