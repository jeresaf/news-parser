<?php

namespace App\Controller\Admin;

use App\Entity\News;
use EasyCorp\Bundle\EasyAdminBundle\Config\Action;
use EasyCorp\Bundle\EasyAdminBundle\Config\Actions;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Config\Filters;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Filter\EntityFilter;

class NewsCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return News::class;
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setEntityLabelInSingular('News Article')
            ->setEntityLabelInPlural('News Articles')
            ->setSearchFields(['title', 'description', 'article'])
            ->setDefaultSort(['date_added' => 'DESC'])
            ->setEntityPermission('ROLE_ADMIN')
            ->setPaginatorPageSize(10)
            ->setPaginatorRangeSize(3)
            ->setPaginatorUseOutputWalkers(true)
            ->setPaginatorFetchJoinCollection(true)
        ;
    }

    public function configureActions(Actions $actions): Actions
    {
        return $actions
            ->remove(Crud::PAGE_INDEX, Action::NEW)
            ->remove(Crud::PAGE_INDEX, Action::EDIT)
            ->remove(Crud::PAGE_DETAIL, Action::EDIT)
            ;
    }

     public function configureFields(string $pageName): iterable
    {
        yield TextField::new('title');
        yield TextareaField::new('description')->hideOnIndex();
        yield TextareaField::new('article')->hideOnIndex();
        yield ImageField::new('picture')
            ->setBasePath('/uploads/pictures')
            ->setLabel('Photo')
            ->hideOnIndex();

        $date_added = DateTimeField::new('date_added')->setFormTypeOptions([
            'html5' => true,
            'years' => range(date('Y'), date('Y') + 5),
            'widget' => 'single_text',
        ]);
        if (Crud::PAGE_EDIT === $pageName || Crud::PAGE_NEW === $pageName) {
            yield $date_added->setFormTypeOption('disabled', true);
        } else {
            yield $date_added;
        }

        $date_updated = DateTimeField::new('date_updated')->setFormTypeOptions([
            'html5' => true,
            'years' => range(date('Y'), date('Y') + 5),
            'widget' => 'single_text',
        ]);
        if (Crud::PAGE_EDIT === $pageName || Crud::PAGE_NEW === $pageName) {
            yield $date_updated->setFormTypeOption('disabled', true);
        } else {
            yield $date_updated;
        }
     }

}
