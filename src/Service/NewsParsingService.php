<?php

namespace App\Service;

use App\Entity\News;
use App\Entity\NewsMetaData;
use App\Repository\NewsMetaDataRepository;
use App\Repository\NewsRepository;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpClient\CurlHttpClient;
use Symfony\Contracts\HttpClient\ResponseInterface;

class NewsParsingService
{
    private $logger;
    private $client;
    private $doctrine;
    private $newsRepository;
    private $truncateSentencesService;
    private $newsMetaDataRepository;

    public function __construct(LoggerInterface $logger, ManagerRegistry $doctrine, NewsRepository $newsRepository,
                                TruncateSentencesService  $truncateSentencesService, NewsMetaDataRepository $newsMetaDataRepository)
    {
        $this->client = new CurlHttpClient();
        $this->logger = $logger;
        $this->doctrine = $doctrine;
        $this->newsRepository = $newsRepository;
        $this->truncateSentencesService = $truncateSentencesService;
        $this->newsMetaDataRepository = $newsMetaDataRepository;
    }

    public function parseNews(): void
    {

        $newsMetaData = $this->newsMetaDataRepository->find(1);
        if(isset($newsMetaData)) {
            $url = $newsMetaData->getNextUrl();
            $url .= '&x-api-key=hlv89QE22cCD9G1aU1aIZW8j92b6bTcu';
        } else {
            $url = 'https://get.scrapehero.com/news-api/news/?x-api-key=hlv89QE22cCD9G1aU1aIZW8j92b6bTcu&q=Iphone&sentiment=Positive&start_date=2020-12-01&end_date=2022-12-03&source_id=277%2C4171&category_id=13010000%2C04018000';
        }

        dump($url);

        //https://get.scrapehero.com/news-api/news/?x-api-key=XXXXXXXXXXXXXX&q=Iphone&sentiment=Positive&start_date=2020-12-01&end_date=2020-12-03&source_id=277%2C4171&category_id=13010000%2C04018000
        $response = $this->client->request(
            'GET',
            $url,
            [
                'extra' => [
                    'curl' => [],
                ],
            ]
        );

        $articles_string = $response->getContent();

        $count = 0;

        if(!empty($articles_string)){

            $entityManager = $this->doctrine->getManager();

            $articles_json = json_decode($articles_string);

            $nextUrl = $articles_json->result->nextUrl;
            if(isset($newsMetaData)) {
                $newsMetaData->setNextUrl($nextUrl);
            } else {
                $newsMetaData = new NewsMetaData();
                $newsMetaData->setNextUrl($nextUrl);

                $entityManager->persist($newsMetaData);
            }
            $entityManager->flush();

            $articles = $articles_json->result->data;
            foreach ($articles as $article) {
                $title = $article->title;

                $db_article = $this->newsRepository->findOneByTitle($title);

                if (!isset($db_article)) {
                    $content = $article->content;
                    $description = $this->truncateSentencesService->truncate($content);
                    $image_url = $article->image_url;
                    $news = new News();
                    $news->setTitle($title);
                    $news->setDescription($description);
                    $news->setArticle($content);
                    $news->setPicture($image_url);

                    $entityManager->persist($news);

                } else {
                    $db_article->setDateUpdatedValue();
                }

                $entityManager->flush();

                $count++;
            }
        }

        $this->logger->info(sprintf('Parsed "%d" news articles.', $count));

    }

}