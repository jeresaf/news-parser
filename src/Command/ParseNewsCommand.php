<?php

namespace App\Command;

use App\Entity\News;
use App\Message\ParseNews;
use App\Repository\NewsRepository;
use App\Service\NewsParsingService;
use App\Service\TruncateSentencesService;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Messenger\MessageBusInterface;

class ParseNewsCommand extends Command
{
//    private $doctrine;
//    private $newsRepository;
//    private $newsParser;
//    private $truncateSentencesService;
    private $bus;

    protected static $defaultName = 'app:news:parse';

    public function __construct(MessageBusInterface $bus)
    {
        $this->bus = $bus;

//        ManagerRegistry $doctrine, NewsRepository $newsRepository, NewsParsingService $newsParser,
//                                TruncateSentencesService  $truncateSentencesService,
//        $this->doctrine = $doctrine;
//        $this->newsRepository = $newsRepository;
//        $this->newsParser = $newsParser;
//        $this->truncateSentencesService = $truncateSentencesService;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Deletes rejected and spam comments from the database')
            ->setHelp('This command allows you to parse news...');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        dump("Pre Parse");

        $this->bus->dispatch(new ParseNews());

        dump("Post Parse");

//        $news_response = $this->newsParser->parseNews();
//        $articles_string = $news_response->getContent();
//
//        $count = 0;
//
//        if(!empty($articles_string)){
//
//            $entityManager = $this->doctrine->getManager();
//
//            $articles_json = json_decode($articles_string);
//            $articles = $articles_json->result->data;
//            foreach ($articles as $article) {
//                $title = $article->title;
//
//                $db_article = $this->newsRepository->findOneByTitle($title);
//
//                if (!isset($db_article)) {
//                    $content = $article->content;
//                    $description = $this->truncateSentencesService->truncate($content);
//                    $image_url = $article->image_url;
//                    $news = new News();
//                    $news->setTitle($title);
//                    $news->setDescription($description);
//                    $news->setArticle($content);
//                    $news->setPicture($image_url);
//
//                    $entityManager->persist($news);
//
//                } else {
//                    $db_article->setDateUpdatedValue();
//                }
//
//                $entityManager->flush();
//
//                $count++;
//            }
//        }

        $io = new SymfonyStyle($input, $output);

        $io->success('Parsing News Command Completed.');

        return Command::SUCCESS;
    }
}