<?php

namespace App\Entity;

use App\Repository\NewsMetaDataRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=NewsMetaDataRepository::class)
 */
class NewsMetaData
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=1000, nullable=true)
     */
    private $nextUrl;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNextUrl(): ?string
    {
        return $this->nextUrl;
    }

    public function setNextUrl(?string $nextUrl): self
    {
        $this->nextUrl = $nextUrl;

        return $this;
    }
}
