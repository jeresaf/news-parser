# News Parser



## Setting Up The Project
The project is run using docker and all setting up is done using the docker terminal

First clone the project
```
git clone https://gitlab.com/jeresaf/news-parser.git
```

Start the docker containers

```
docker-compose up -d --build
```

Start the docker containers

```
docker-compose up -d --build
```

Run docker terminal

```
docker-compose exec php /bin/bash
```

Install composer dependencies

```
composer install
```

Install yarn dependencies

```
symfony run yarn dev
```

Create mysql 8.0 database

```
"CREATE DATABASE app"
```

Migrate tables

```
symfony console doctrine:migrations:migrate
```

Run the rabbitmq workers

```
php bin/console messenger:consume async
```
